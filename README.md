```
      _                 _                       __ _       
  ___(_)_ __ ___  _ __ | | ___  ___ ___  _ __  / _(_) __ _ 
 / __| | '_ ` _ \| '_ \| |/ _ \/ __/ _ \| '_ \| |_| |/ _` |
 \__ \ | | | | | | |_) | |  __/ (_| (_) | | | |  _| | (_| |
 |___/_|_| |_| |_| .__/|_|\___|\___\___/|_| |_|_| |_|\__, |
                 |_|                                 |___/ 
```
Simple [OpenWrt](https://openwrt.org/) configuration via remote [ubus](https://openwrt.org/docs/techref/ubus).
Uses the [NetJSON](https://netjson.org) [DeviceConfiguration format](https://netjson.org/rfc.html#section-5).

### How to install simpleconfig ?

1. Install Simplediscovery: see https://codeberg.org/tob/simplediscovery.git
2. Add `src-git simpleconfig https://codeberg.org/tob/simpleconfig.git` to your [feeds.conf](https://openwrt.org/docs/guide-developer/feeds)
3. Run `./scripts/feeds update simpleconfig`
4. Run `./scripts/feeds install simpleconfig`
5. Run `make menuconfig` and select simpleconfig under network
6. Run `make package/simpleconfig/compile` to create only the simpleconfig package or `make` to create a new image

To make Simpleconfig available via [ubus](https://openwrt.org/docs/techref/ubus), it uses [rpcd's](https://openwrt.org/docs/techref/rpcd) plugin executables functionality. The executable is stored in `/usr/libexec/rpcd/simpleconfig`.

### How to use simpleconfig ?

Call simpleconfig with `ubus call simpleconfig get_wifi`
or
```
ubus call simpleconfig set_wifi '{ "type": "DeviceConfiguration", "interfaces": [ { "name": "wlan0", "type": "wireless", "wireless": { "mode": "ap", "ssid": "<my_new_ssid>", "radio": "<radio0>", "encryption": { "key": "<my_new_secret_key>" } } } ] }'
```

Sample output of `ubus call simpleconfig get_wifi` looks like this:
```
{
	"type":"DeviceConfiguration",
	"radios": [{
		"name": "radio0",
		"protocol": "802.11ac",
		"channel": 36,
		"channel_width": 80
	}],
	"interfaces":[
		{
			"name":"wlan0",
			"type":"wireless",
			"wireless":{
				"mode":"access_point",
				"ssid":"OpenWrt",
				"radio":"radio0",
				"network": [
					"lan"
				],
				"encryption":{
					"key":"secret_key_123",
					"protocol":"psk2"
				}
			}
		}
	]
}
```

List of available calls:

* "get_wifi":{"no_cache":"[0,1]"}
* "set_wifi":{NetJSON Deviceconfig}
* "add_wifi":{NetJSON Deviceconfig}
* "delete_wifi":{NetJSON Deviceconfig}
* "global_get_wifi":{"no_cache":"[0,1]"}
* "global_set_wifi":{List of NetJSON Deviceconfigs}
* "global_add_wifi":{List of NetJSON Deviceconfigs}
* "global_delete_wifi":{List of NetJSON Deviceconfigs}
* "global_repeat_ssid":{"ssid":"String","except":"String","validation":"[0,1]"}
* "global_change_wifi_password":{"ssid":"String","password":"String","validation":"[0,1]"}
* "global_change_wifi_ssid":{"ssid":"String","new_ssid":"String","validation":"[0,1]"}
* "global_set_config_password":{"password":"String"}

