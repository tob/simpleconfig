#!/bin/sh
. /usr/share/libubox/jshn.sh

# functions using ubus via http


ubus_login() {
	local ip=$1
	local username=$2
	local password=$3

	local result="$(wget --no-check-certificate -T $timeout -q -O-\
		      --post-data="\
		        {\"params\": \
		          [ \"00000000000000000000000000000000\", \"session\", \"login\", { \"username\": \"${username}\", \"password\": \"${password}\"  } ], \
		        \"method\": \"call\", \
		        \"id\": \"1\", \
		        \"jsonrpc\": \"2.0\" }" \
		      "http://${ip}/ubus")"

	if [ -z "$result" ]; then
		echo ""
		return "1"
	fi
	json_load "$result"
	json_select result
	json_get_var return_code 1
	if [ "$return_code" = '0' ]; then
		json_select "2"
		json_get_var token ubus_rpc_session
		echo "$token"
	else
		echo "$return_code"
		return 0
	fi
}

ubus_remote_call() {
	local ip=$1
	local username=$2
	local password=$3
	local target=$4
	local command=$5
	local parameter=$6
	timeout=$7
	local token=$8 #optional

	if [ -z "$timeout"  ]; then
		timeout=2
	fi

	if [ -z "$token" ]; then
		local token="$(ubus_login ${ip} ${username} ${password})" 
		
		if [ -z "$token" ]; then
			echo ""
			return 1
		fi
		
		if [ "$token" = '6' ]; then
			if [ "$target" = 'simpleconfig' ] && [ "password" != 'simpleconfig' ]; then 
				ubus_remote_call "$ip" "$username" "simpleconfig" "$target" "$command" "$parameter"
				return
			else
				echo "$token"
				return "$token"
			fi
		fi
	fi

	local result="$(wget --no-check-certificate -T ${timeout} -q -O-\
		      --post-data="\
		        {\"params\": \
		          [ \"${token}\", \"${target}\", \"${command}\", ${parameter} ], \
		        \"method\": \"call\", \
		        \"id\": \"1\", \
		        \"jsonrpc\": \"2.0\" }" \
		      "http://${ip}/ubus")"

	json_load "$result"
	json_select result
	json_get_var return_code 1
	if [ "$return_code" = '0' ]; then
		result="$(echo "$result" | awk -F '\\[[0-9],' '{ print $2 }' )"
		result=${result%]*} # deletes from last occurence of ] to end of string
		echo "$result"
	else
		echo "$result"
		return 0
	fi
}
