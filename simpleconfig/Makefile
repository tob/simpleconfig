#
# Copyright (C) 2021 Tobias Ilte
#
# This is free software, licensed under the GNU General Public License v3 or later.

include $(TOPDIR)/rules.mk

PKG_NAME:=simpleconfig
PKG_VERSION:=1.0
PKG_RELEASE:=1
PKG_MAINTAINER:=Tobias Ilte <tobias.ilte@campus.tu-berlin.de>
PKG_LICENSE:=GPL-3.0+
PKG_LICENSE_FILES:=LICENSE

include $(INCLUDE_DIR)/package.mk

define Package/simpleconfig
  SECTION:=net
  CATEGORY:=Network
  DEPENDS:=+uhttpd-mod-ubus +rpcd +rpcd-mod-file +jq +simplediscovery
  MAINTAINER:=Tobias Ilte <tobias.ilte@campus.tu-berlin.de>
  TITLE:=Simple configuration via remote ubus using NetJSON
endef

define Package/simpleconfig/description
  Get and set config via ubus using the NetJSON DeviceConfiguration format
endef

define Package/simpleconfig/conffiles
	/etc/config/simpleconfig
endef

define Build/Configure
endef

define Build/Compile
endef

define Package/simpleconfig/install
	$(INSTALL_DIR) $(1)/usr/libexec/rpcd
	$(INSTALL_BIN) ./src/simpleconfig $(1)/usr/libexec/rpcd/simpleconfig
	$(INSTALL_DIR) $(1)/usr/share
	$(INSTALL_BIN) ./src/libremoteubus.sh $(1)/usr/share/libremoteubus.sh
	$(INSTALL_DIR) $(1)/usr/sbin
	$(INSTALL_BIN) ./src/changepw $(1)/usr/sbin/changepw
	$(INSTALL_DIR) $(1)/usr/bin
	$(INSTALL_BIN) ./src/simpleconfig-cache $(1)/usr/bin/simpleconfig-cache
	$(INSTALL_DIR) $(1)/usr/share/rpcd/acl.d
	$(INSTALL_DATA) ./files/simpleconfig.json $(1)/usr/share/rpcd/acl.d/simpleconfig.json
	$(INSTALL_DATA) ./files/uci.json $(1)/usr/share/rpcd/acl.d/uci.json
	$(INSTALL_DATA) ./files/file.json $(1)/usr/share/rpcd/acl.d/file.json
	$(INSTALL_DIR) $(1)/etc/uci-defaults
	$(INSTALL_BIN) ./files/simpleconfig.defaults $(1)/etc/uci-defaults/simpleconfig-setup
endef

$(eval $(call BuildPackage,simpleconfig))
